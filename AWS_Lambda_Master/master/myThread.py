import threading
import time
import boto3
import json

class myThread (threading.Thread):
   def __init__(self, threadID, num_workers, t, name, result):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.num_workers = num_workers
        self.name = name
        self.t = t
        self.result = result
        
   def run(self):
        print ("Starting " + self.name)
        lambda_client = boto3.client('lambda')
        lambda_payload = {"argv1" : self.threadID, "argv2" : self.num_workers, "argv3" : self.t}
        response = lambda_client.invoke(FunctionName='Worker', 
                     InvocationType='RequestResponse',
                     Payload=json.dumps(lambda_payload))
        rFromW = json.load(response['Payload'])
        self.result = rFromW['body']
        return rFromW

