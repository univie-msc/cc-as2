import hashlib
import time
from random import random
from aws.helper import Helper, MSG_GROUP_TYPE_INITIAL
from aws.sns import Sns
from master.myThread import myThread
from aws.s3 import S3
import numpy as np
import boto3
import json


class Master:
    def __init__(self, num_workers, num_steps, matrix_width, matrix_height):
        self.num_workers = num_workers
        self.num_steps = num_steps
        self.matrix_width = matrix_width
        self.matrix_height = matrix_height
        self.M = []
        self.s3 = S3('cc2020w')

    def init_matrix(self):
        self.M = np.zeros([self.matrix_height, self.matrix_width])
        for i in range(1, self.matrix_height - 1):
            for j in range(1, self.matrix_width - 1):
                self.M[i][j] = j * (self.matrix_width - j - 1) * i * (self.matrix_height - i - 1)

    def distribute_subtasks(self):
        avg_rows = self.matrix_height / self.num_workers
        extra_rows = self.matrix_height % self.num_workers
        offset = 0

        for i in range(self.num_workers):
            # send workers their portion of the matrix
            rows = int(avg_rows + 1 if (i < extra_rows) else avg_rows)
            sub_M = self.M[offset:offset + rows, :]
            encoded_sub_m = Helper.encode_for_transport(sub_M)
            Sns.publish_to_topic(
                Helper.get_topic_name(i),
                encoded_sub_m,
                MSG_GROUP_TYPE_INITIAL,
                hashlib.md5(str(random()).encode()).hexdigest())
            offset = offset + rows

    def start(self):
        Helper.create_queues(self.num_workers)
        Helper.create_topics(self.num_workers)
        Helper.subscribe_to_topics(self.num_workers)
        self.init_matrix()
        #self.distribute_subtasks()

    def iterate_workers(self, delay):
        for t in range(self.num_steps):
            self.distribute_subtasks()

            threads = []

            # Create new threads
            # thread1 = myThread(1, "Thread-1", 1)

            for i in range(self.num_workers):
                threads.append(myThread(i, self.num_workers, t, "Worker" + str(i), ""))

            # Start new Threads
            # thread1.start()

            for y in threads:
                y.start()

            # Add threads to thread list
            # threads.append(thread1)

            # Wait for all threads to complete
            for th in threads:
                th.join()
                
            JSONsReceivedFromThreads = []
            
            """
            for current in threads:
                JSONsReceivedFromThreads.append(current.result)
                print("Time to print results: ")
                print(json.dumps(current.result))
            """
            
            final_res_matrix = None
            for i in range(len(threads)):
                JSONsReceivedFromThreads.append(threads[i].result)
                #print('W' + str(i) + ': ' + json.dumps(threads[i].result))
                res_matrix = np.array(json.loads(threads[i].result))
                
                if len(threads) > 1:  # number of workers
                    if i == 0:
                        res_matrix = res_matrix[0:-1]
                        final_res_matrix = res_matrix
                        print('W' + str(i) + ':\n' + str(res_matrix))
                        continue
                    if i == len(threads) - 1:
                        res_matrix = res_matrix[1:]
                    if i != 0 and i != len(threads) - 1:
                        res_matrix = res_matrix[1:-1]
                    final_res_matrix = np.concatenate((final_res_matrix, res_matrix))
                else:
                    final_res_matrix = res_matrix
                
                print('W' + str(i) + ':\n' + str(res_matrix))
                
            print('FINAL:\n' + str(final_res_matrix))
            
            self.M = final_res_matrix
            
            self.s3.save_to_s3('matrix-' + str(t), final_res_matrix)
            time.sleep(delay)  # delay to make changes visible on the user's side in seconds
            
        return self.M.tolist()

    def cleanup(self):
        Helper.delete_queues(self.num_workers)
        Helper.delete_topics(self.num_workers)

    def test(self):
        print("Master object can be called on method test")
        lambda_client = boto3.client('lambda')
        lambda_payload = ""
        response = lambda_client.invoke(FunctionName='Worker',
                                        InvocationType='RequestResponse',
                                        Payload=json.dumps(lambda_payload))
        rFromW = json.load(response['Payload'])
        print(rFromW)

        threads = []

        # Create new threads
        thread1 = myThread(1, "Thread-1", 1)
        thread2 = myThread(2, "Thread-2", 2)

        # Start new Threads
        thread1.start()
        thread2.start()

        # Add threads to thread list
        threads.append(thread1)
        threads.append(thread2)

        # Wait for all threads to complete
        for t in threads:
            t.join()
