import json
from master.master import Master

def main(argv):
    # TODO delete
    
    num_workers = argv[0]
    num_steps = argv[1]
    matrix_width = argv[2]
    matrix_height = argv[3]
    

    m = Master(num_workers, num_steps, matrix_width, matrix_height)
    m.start()
    M = m.iterate_workers(1)
    #m.cleanup()
    
    return M

def lambda_handler(event, context):
    # TODO implement
    
    argv = [event['argv1'], event['argv2'], event['argv3'], event['argv4']]

    M = main(argv)
    
    return {
        'statusCode': 200,
        'body': json.dumps(M)
    }
