import numpy as np
import boto3


class S3:
    def __init__(self, bucket_name='myunicc'):
        self.bucket_name = bucket_name
        self.s3 = boto3.resource('s3')

    def read_from_s3(self, filename, matrix_shape, matrix_type):
        obj = self.s3.Bucket(self.bucket_name).Object(filename)
        obj = obj.get()['Body']
        temp= np.fromstring(obj.read(), matrix_type).reshape(matrix_shape)
        return temp


    # after creating bucket adjust permissions manually
    def save_to_s3(self, filename, matrix):
        bucket = self.s3.Bucket(self.bucket_name)
        #test if bucket exists
        if bucket.creation_date:
            tmp = matrix.tostring()
            self.s3.Object(self.bucket_name, filename ).put(Body=tmp)
            return "OK"
        else:
            self.s3.create_bucket(Bucket=self.bucket_name, CreateBucketConfiguration={'LocationConstraint': 'us-east-1'})
            #save_to_s3(filename, matrix)
