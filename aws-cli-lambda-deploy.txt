aws lambda list-functions --query '[Functions[].[FunctionName,FunctionArn]]'


MASTER

virtualenv --python=python3 myvenv
source myvenv/bin/activate
pip install boto3 numpy
deactivate
cd myvenv/lib/python3.6/site-packages
zip -r master.zip .
mv -f master.zip ../../../../master.zip
cd ../../../../
zip -g master.zip lambda_function.py master/* aws/*
aws lambda update-function-code --function-name Master --zip-file fileb://master.zip
aws lambda invoke --function-name Master --payload '{"argv1": 1,"argv2": 1,"argv3": 10,"argv4": 10}' out --log-type Tail --query 'LogResult' --output text |  base64 -d


WORKER

virtualenv --python=python3 myvenv
source myvenv/bin/activate
pip install boto3 numpy
deactivate
cd myvenv/lib/python3.6/site-packages
zip -r worker.zip .
mv -f worker.zip ../../../../worker.zip
cd ../../../../
zip -g worker.zip lambda_function.py worker/* aws/*
aws lambda update-function-code --function-name Worker --zip-file fileb://worker.zip
aws lambda invoke --function-name Worker out --log-type Tail --query 'LogResult' --output text |  base64 -d


