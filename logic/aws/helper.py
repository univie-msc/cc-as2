import numpy as np
import json
from aws.sns import Sns
from aws.sqs import Sqs


MSG_GROUP_TYPE_INITIAL = 'initial'
MSG_GROUP_TYPE_NORTH = 'north'
MSG_GROUP_TYPE_SOUTH = 'south'


class Helper:
    @staticmethod
    def create_queues(num_workers):
        for worker in range(num_workers):
            Sqs.create_queue('w_' + str(worker) + '.fifo')

    @staticmethod
    def create_topics(num_workers):
        for worker in range(num_workers):
            Sns.create_or_get_topic('w_' + str(worker) + '.fifo')  # for initial subarrays
            Sns.create_or_get_topic('w_' + str(worker) + '_north.fifo')  # for northern neighbors
            Sns.create_or_get_topic('w_' + str(worker) + '_south.fifo')  # for southern neighbors

    @staticmethod
    def delete_queues(num_workers):
        for worker in range(num_workers):
            Sqs.delete_queue('w_' + str(worker) + '.fifo')

    @staticmethod
    def delete_topics(num_workers):
        for worker in range(num_workers):
            Sns.delete_topic('w_' + str(worker) + '.fifo')
            Sns.delete_topic('w_' + str(worker) + '_north.fifo')
            Sns.delete_topic('w_' + str(worker) + '_south.fifo')

    @staticmethod
    def subscribe_to_topics(num_workers):
        for worker in range(num_workers):
            queue_arn = Sqs.get_queue_arn('w_' + str(worker) + '.fifo')
            Sns.subscribe_to_topic('w_' + str(worker) + '.fifo', queue_arn)
            if worker > 0:
                Sns.subscribe_to_topic('w_' + str(worker - 1) + '_south.fifo', queue_arn)
            if worker < num_workers - 1:
                Sns.subscribe_to_topic('w_' + str(worker + 1) + '_north.fifo', queue_arn)

    @staticmethod
    def get_topic_name(worker_num, neighbor=None):
        return 'w_' + str(worker_num) + (('_' + neighbor) if (neighbor is not None) else '') + '.fifo'

    @staticmethod
    def get_queue_name(worker_num):
        return 'w_' + str(worker_num) + '.fifo'

    @staticmethod
    def encode_for_transport(data, type='JSON'):
        return json.dumps(data.tolist())

    @staticmethod
    def decode_from_transport(data, type='JSON'):
        return np.array(json.loads(data))
