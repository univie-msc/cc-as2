import boto3


class Sqs:
    def __init__(self, queue_name, queue_type='fifo'):
        # self.queue_name = queue_name + ('.' + queue_type) if queue_type == 'fifo' else ''
        self.queue_name = queue_name
        self.queue_type = queue_type
        self.sqs = boto3.client('sqs')

    @staticmethod
    def create_queue(queue_name, queue_type='fifo'):
        sqs = boto3.client('sqs')
        response = sqs.create_queue(
            QueueName=queue_name,
            Attributes={
                'FifoQueue': 'true' if queue_type == 'fifo' else 'false',
                'DelaySeconds': '0'
            }
        )
        Sqs.allow_sns_to_write_to_sqs(queue_name)

    @staticmethod
    def delete_queue(queue_name):
        sqs = boto3.client('sqs')
        queue_url = Sqs.get_queue_url(queue_name)
        response = sqs.delete_queue(
            QueueUrl=queue_url
        )

    @staticmethod
    def allow_sns_to_write_to_sqs(queue_name):
        sqs = boto3.client('sqs')
        queue_url = Sqs.get_queue_url(queue_name)
        queue_arn = Sqs.get_queue_arn(queue_name)
        policy = """{{
        "Version": "2008-10-17",
          "Id": "__default_policy_ID",
          "Statement": [
            {{
              "Sid": "__owner_statement",
              "Effect": "Allow",
              "Principal": {{
                "Service": "sns.amazonaws.com"
              }},
              "Action": "SQS:*",
              "Resource": "{0}"
            }}
          ]
        }}""".format(queue_arn)

        response = sqs.set_queue_attributes(
            QueueUrl=queue_url,
            Attributes={
                'Policy': policy
            }
        )

    @staticmethod
    def get_queue_arn(queue_name):
        sqs = boto3.client('sqs')
        queue_url = Sqs.get_queue_url(queue_name)
        response = sqs.get_queue_attributes(
            QueueUrl=queue_url,
            AttributeNames=[
                'QueueArn'
            ]
        )
        return response['Attributes']['QueueArn']

    @staticmethod
    def get_queue_url(queue_name):
        sqs = boto3.client('sqs')
        response = sqs.get_queue_url(QueueName=queue_name)
        return response['QueueUrl']

    def delete_messages_from_queue(self, messages: list):
        queue_url = Sqs.get_queue_url(self.queue_name)
        for message in messages:
            self.sqs.delete_message(
                QueueUrl=queue_url,
                ReceiptHandle=message['ReceiptHandle']
            )

    def get_message_from_queue(self, num_messages=1, delete=True, wait_time_seconds=0):
        queue_url = Sqs.get_queue_url(self.queue_name)
        response = self.sqs.receive_message(
            QueueUrl=queue_url,
            AttributeNames=[
                'SentTimestamp',
                'MessageGroupId'
            ],
            MaxNumberOfMessages=num_messages,
            MessageAttributeNames=[
                'All'
            ],
            VisibilityTimeout=5,
            WaitTimeSeconds=wait_time_seconds
        )
        if 'Messages' in response:
            """
            all_messages = response['Messages']
            messages = []
            # search by message_group_id
            if message_group_id is not None:
                for msg in all_messages:
                    if msg['Attributes']['MessageGroupId'] == str(message_group_id):
                        messages.append(msg)
            else:
                messages = all_messages
            """
            if delete is True:
                self.delete_messages_from_queue(response['Messages'])
            return response['Messages']
        else:
            return None
