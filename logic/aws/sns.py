import boto3


class Sns:
    @staticmethod
    def create_or_get_topic(topic_name, topic_type='fifo'):
        sns = boto3.client('sns')
        response = sns.create_topic(
            # Name=topic_name + ('.' + topic_type) if topic_type == 'fifo' else '',
            Name=topic_name,
            Attributes={
                'FifoTopic': 'true' if topic_type == 'fifo' else 'false'
            }
        )
        return response

    @staticmethod
    def delete_topic(topic_name):
        sns = boto3.client('sns')
        topic_arn = Sns.create_or_get_topic(topic_name)['TopicArn']
        response = sns.delete_topic(
            TopicArn=topic_arn
        )

    @staticmethod
    def subscribe_to_topic(topic_name, queue_arn):
        sns = boto3.client('sns')
        topic_arn = Sns.create_or_get_topic(topic_name)['TopicArn']
        response = sns.subscribe(
            TopicArn=topic_arn,
            Protocol='sqs',
            Endpoint=queue_arn
        )

    @staticmethod
    def publish_to_topic(topic_name, message, message_group_id, message_duplication_id, topic_type='fifo'):
        sns = boto3.client('sns')
        topic_arn = Sns.create_or_get_topic(topic_name, topic_type)['TopicArn']
        sns.publish(TopicArn=topic_arn,
                    Message=message,
                    MessageGroupId=str(message_group_id),
                    MessageDeduplicationId=str(message_duplication_id))
