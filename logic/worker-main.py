import sys

from worker.worker import Worker


def main(argv):
    # TODO delete
    argv = [0, 1, 10, 0.1, 0.1]

    worker_num = argv[0]
    num_workers = argv[1]
    num_steps = argv[2]
    cx = argv[3]
    cy = argv[4]

    # This is a serial test
    w = Worker(worker_num, num_workers, cx, cy)

    for t in range(num_steps):
        w.do_job(t)


if __name__ == "__main__":
    main(sys.argv[1:])
