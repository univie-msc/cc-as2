from aws.s3 import S3
from master.master import Master
from worker.worker import Worker
import time


def logic_test():
    worker_num = 0
    num_workers = 1
    num_steps = 1
    cx = 0.1
    cy = 0.1
    matrix_width = 10
    matrix_height = 10

    m = Master(num_workers, num_steps, matrix_width, matrix_height)
    m.start()
    # m.iterate_workers(1)
    # m.cleanup()

    # iterate_workers
    for t in range(num_steps):
        m.distribute_subtasks()

        w = Worker(worker_num, num_workers, cx, cy, t)
        M = w.do_job()

        s3 = S3('cc2020w')
        s3.save_to_s3('matrix-' + str(t), M)

        time.sleep(1)  # delay to make changes visible on the user's side in seconds


if __name__ == "__main__":
    logic_test()
