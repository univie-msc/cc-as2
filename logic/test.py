import hashlib
from random import random
import numpy as np
from aws.helper import Helper
from aws.sns import Sns
from aws.sqs import Sqs
import json


def test():
    queue_name = 'queuePyTest.fifo'
    topic_name = 'topicPyTest.fifo'

    if True is False:
        print('Delete queue...')
        try:
            Sqs.delete_queue(queue_name)
        except:
            print('Error')

        print('Delete topic...')
        try:
            Sns.delete_topic(topic_name)
        except:
            print('Error')

    print('Create queue...')
    Sqs.create_queue(queue_name)

    print('Create topic...')
    Sns.create_or_get_topic(topic_name)

    print('Subscribe to topic...')
    queue_arn = Sqs.get_queue_arn(queue_name)
    Sns.subscribe_to_topic(topic_name, queue_arn)

    print('Publish to topic...')
    matrix = np.zeros([3, 4])
    message_group_id = 1
    message_duplication_id = hashlib.md5(str(random()).encode()).hexdigest()
    Sns.publish_to_topic(topic_name, Helper.encode_for_transport(matrix), message_group_id, message_duplication_id)
    message_group_id = 2
    message_duplication_id = hashlib.md5(str(random()).encode()).hexdigest()
    Sns.publish_to_topic(topic_name, Helper.encode_for_transport(matrix), message_group_id, message_duplication_id)

    my_queue = Sqs(queue_name)

    print('Retrieve messages...')
    #try:
    messages = my_queue.get_message_from_queue(num_messages=1, delete=True, wait_time_seconds=20)
    if messages == 1:
        print('No message received. Terminating...')
        exit()
    msg_body = json.loads(messages[-1]['Body'])
    data = np.array(json.loads(msg_body['Message']))
    #except Exception as e:
    #    print('Error: ' + str(e))

    print(data)

    exit()


if __name__ == "__main__":
    test()
