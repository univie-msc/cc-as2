import hashlib
from random import random
import numpy as np
from aws.helper import Helper, MSG_GROUP_TYPE_NORTH, MSG_GROUP_TYPE_SOUTH
from aws.sns import Sns
from aws.sqs import Sqs
import json


class Worker:
    def __init__(self, worker_num, num_workers, cx, cy, t):
        self.worker_num = worker_num
        self.num_workers = num_workers
        self.cx = cx
        self.cy = cy
        self.sqs = Sqs(Helper.get_queue_name(worker_num))
        self.t = t

    def get_data(self):
        try:
            messages = self.sqs.get_message_from_queue(num_messages=1, delete=True, wait_time_seconds=20)
            if messages is None:  # No message received
                return None
            msg_body = json.loads(messages[0]['Body'])
            return np.array(json.loads(msg_body['Message']))
        except Exception as e:
            print('Error: ' + str(e))
            return None

    def update(self, M, start, end, width):
        M_new = np.array(M, copy=True)

        for r in range(start + 1, end):
            for c in range(1, width - 1):
                M_new[r][c] = M[r][c] + \
                    self.cx * (M[r][c + 1] + M[r][c - 1] - 2 * M[r][c]) + \
                    self.cy * (M[r + 1][c] + M[r - 1][c] - 2 * M[r][c])

        return M_new

    def do_job(self):
        """
        width = 15
        height = 15
        M = np.zeros([height, width])
        for i in range(1, height - 1):
            for j in range(1, width - 1):
                M[i][j] = j * (width - j - 1) * i * (height - i - 1)
        """

        M = self.get_data()
        if M is None:
            print('Error: no initial message received at W' + str(self.worker_num))
            exit(1)

        start = 0  # row start
        end = np.shape(M)[0] - 1  # row end
        width = np.shape(M)[1]

        num_steps = 1  # iterations are not done here, it is only for debugging purposes
        for t in range(num_steps):
            # I have a northern neighbor - send my north and get neighbor's south
            if self.worker_num > 0:
                northern_array = Helper.encode_for_transport(M[start, :])
                Sns.publish_to_topic(
                    Helper.get_topic_name(self.worker_num, 'north'),
                    northern_array,
                    MSG_GROUP_TYPE_NORTH,
                    hashlib.md5(str(random()).encode()).hexdigest())
                south = self.get_data()
                if south is None:
                    print('Error: no neighbor south received at W' + str(self.worker_num))
                    exit(1)
                south = np.array([south])
                M = np.concatenate((south, M))

            # I have a southern neighbor - send my south and get neighbor's north
            if self.worker_num < self.num_workers - 1:
                southern_array = Helper.encode_for_transport(M[end, :])
                Sns.publish_to_topic(
                    Helper.get_topic_name(self.worker_num, 'south'),
                    southern_array,
                    MSG_GROUP_TYPE_SOUTH,
                    hashlib.md5(str(random()).encode()).hexdigest())
                north = self.get_data()
                if north is None:
                    print('Error: no neighbor north received at W' + str(self.worker_num))
                    exit(1)
                north = np.array([north])
                M = np.concatenate((M, north))

            end = np.shape(M)[0] - 1  # row end

            M = self.update(M, start, end, width)

        return M

    def test(self):
        print("Worker can be created and test method called")
