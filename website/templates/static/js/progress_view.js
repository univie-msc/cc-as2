

console.log(document.domain)
console.log(location.port)


window.onload = function() {
    var form = document.getElementById("dim_form");
    form.onsubmit = submitted.bind(form);
}

function submitted(event) {
    event.preventDefault();
    let text_input_x = $( 'input.text-input-x' ).val()
    let text_input_y = $( 'input.text-input-y' ).val()
    msg = JSON.stringify({"input_x" : text_input_x, "input_y" : text_input_y})
    console.log(msg)

	const response = new XMLHttpRequest();
	response.open("POST", "http://" + document.domain + ':' + location.port+"/dimensions");
	response.send(msg);
	response.onload = (e) => {
	    console.log(response.response);
	}

}



const get_matrix = async () => {
  const response = await fetch("http://" + document.domain + ':' + location.port+"/update");
  const json_response = await response.json();
  console.log('json_response')
  console.log(json_response)
  update(json_response)
}


setInterval(get_matrix, 5000);


var width = 1000
var height = 1000

var canvas = d3.select('#container')
  .append('canvas')
  .attr('width', width)
  .attr('height', height);

var context = canvas.node().getContext('2d');



var customBase = document.createElement('custom');
var custom = d3.select(customBase); 


var cellSize = 10;
var groupSpacing = 4;
var cellSpacing = 2;
var offsetTop = height / 5;



function find_x_coords(container,len,value) {
			return 0 + (cellSpacing + cellSize) * value + (cellSpacing + cellSize) * len * container + container * cellSpacing + 50;
		}

function find_y_coords(container,row,value) {
			return 0 + (cellSpacing + cellSize) * row
		}


function pick_color(value) {
	heat = 0
	// heat ranges from 50 to -50
	if (value > 50)
		heat = 50;
	else if (value < -50)
		heat = -50;
	else
		heat = value
	heat += 50;
	heat /= 100
	return d3.interpolateSpectral(heat)

}


function databind(data) {


	var join = custom.selectAll('custom.rect').data(data);

    for (container in data) {

    	//console.log(data[container])
    	for (row in data[container]["container"]) {

    		var len = data[container]["container"][row]["row"].length
    		//console.log(len)

    		for (value in data[container]["container"][row]["row"]) {
    			val = data[container]["container"][row]["row"][value];

	    	var x_coords = find_x_coords(container,len,value);
	    	var y_coords = find_y_coords(container,row,value);
	    	//console.log(x_coords, y_coords)

			var enterSel = join.enter()
			.append('custom')
			.attr('class', 'rect')
			.attr("x", x_coords)
			.attr("y", y_coords)
			.attr('width', 0)
			.attr('height', 0);


			join
			.merge(enterSel)
			.transition()
			.attr('width', cellSize)
			.attr('height', cellSize)
			.attr('fillStyle', function(d) { return pick_color(val); });

		}

		}
		
    }

	var exitSel = join.exit()
	.transition()
	.attr('width', 0)
	.attr('height', 0)
	.remove();

};




function draw() {


      context.clearRect(0, 0, width, height);


      var elements = custom.selectAll('custom.rect') 
      elements.each(function(d,i) {


        var node = d3.select(this); 
        context.fillStyle = node.attr('fillStyle');
        context.fillRect(node.attr('x'), node.attr('y'), node.attr('width'), node.attr('height')) 

      });
};






function update(data_matrix) {

	var len = data_matrix[0]["container"][0]["row"].length
	var len_y = data_matrix[0]["container"].length

	cellSize = Math.min(Math.floor((width - 11 * groupSpacing) / len) - cellSpacing, Math.floor((height - 11 * groupSpacing) / len_y) - cellSpacing);



	d3.select('custom').selectAll('*').remove();
	custom.selectAll("*").remove();



	customBase = document.createElement('custom');
	custom = d3.select(customBase);

	databind(data_matrix); 

	var t = d3.timer(function(elapsed) {
		draw();
		if (elapsed > 300) t.stop();
	});
}

