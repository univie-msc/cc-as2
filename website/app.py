import flask
import json
import os
import random
import math
import requests
import numpy

from aws.s3 import S3
from flask_bootstrap import Bootstrap

app = flask.Flask(__name__, static_folder=os.path.abspath('templates/static'))
app.secret_key = b'4387520935793'

bootstrap = Bootstrap(app)
basedir = os.path.abspath(os.path.dirname(__file__))

x = 3
y = 3
s3 = S3(bucket_name="cc2020w")
t = 0
old_matrix = []

def get_matrix():
	# generate a matrix
	# placeholder until matrix can be loaded from database
	num = random.randint(-50, 19)
	matrix = [{"container": [{"row": [*range(num, num+x)]} for i in range(y)]}]
	return matrix


def get_random_matrix():

	# generate a matrix
	# placeholder until matrix can be loaded from database
	matrix = [{"container": [{"row": [random.randint(-50, 50) for j in range(x)]} for i in range(y)]}]
	return matrix


def read_matrix():
	global t
	try:
		filename = 'matrix-' + str(t)+'.txt'
		matrix_shape = (y, x)
		matrix_type = "int64"
		print(filename)
		m = s3.read_from_s3(filename, matrix_shape, matrix_type)
		t+=1
		return m
	except Exception as e:
		print("Exception:", e)

@app.route('/')
def heatmap():
	return flask.render_template('index.html')


def update_old_matrix():

	global x, y

	old_matrix = [{"container": [{"row": [0] * x} for i in range(y)]}]
	old_matrix = json.dumps(old_matrix)

	return old_matrix


@app.route('/update', methods=['GET'])
def get_matrix_update():

	global old_matrix

	if old_matrix == []:
		old_matrix = update_old_matrix()

	m = read_matrix()
	print(m)
	if m is None:
		return old_matrix
	else:
		m = m.tolist()
		matrix = [{"container": [{"row": [*line]} for line in m]}]

		json_matrix = json.dumps(matrix)
		old_matrix = json_matrix
		return json_matrix



def send_request(x, y):

	num_workers = int((y-1)/10)+1

	msg = 	{
		  "argv1": num_workers,
		  "argv2": 20,
		  "argv3": x,
		  "argv4": y
		}

	#res = requests.post('https://z1reoj1pwd.execute-api.us-east-1.amazonaws.com/test/master', json=msg)
	res = requests.post('https://0o5ypwr6b1.execute-api.us-east-1.amazonaws.com/Deployment/master', json=msg)
	print ('response from server:',res)
	#dictFromServer = res.json()




@app.route('/dimensions', methods=['GET', 'POST'])
def set_matrix_dimensions():

	global x, y, old_matrix

	error = None
	if flask.request.method == 'POST':
		try:
			j = json.loads(flask.request.data)
			input_x = j['input_x']
			input_y = j['input_y']

			if input_x.isnumeric() and input_y.isnumeric():
				if int(input_x) > 100: x = 100
				elif int(input_x) <= 10: x = 10
				else: x = int(input_x)

				if int(input_y) > 100: y = 100
				elif int(input_y) <= 10: y = 10
				else: y = int(input_y)

				old_matrix = update_old_matrix()

				send_request(x, y)

			return "OK"
		except:
			return "ERROR"

	return "ERROR"


@app.route('/test', methods=['GET', 'POST'])
def test():


	error = None
	if flask.request.method == 'POST':
		try:
			print(flask.request.data)
			return "OK"
		except:
			return "ERROR"


	return "ERROR"





if __name__ == '__main__':
	#app.run(debug=True, host='0.0.0.0', port='8080')
	app.run(debug=True, host='0.0.0.0', port='5000')



# build with:
# docker-compose build

# run with:
# docker-compose run --service-ports app
