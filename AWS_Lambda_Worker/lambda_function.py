import json
from worker.worker import Worker


def main(argv):
    # TODO delete

    worker_num = argv[0]
    num_workers = argv[1]
    cx = 0.1
    cy = 0.1
    t = argv[2]

    # This is a serial test
    w = Worker(worker_num, num_workers, cx, cy, t)

    matrix = w.do_job()
    
    
    return {
        'statusCode': 200,
        'body': json.dumps(matrix.tolist())
    }
    
    
        

def lambda_handler(event, context):
    # TODO implement
    argv = [event['argv1'], event['argv2'], event['argv3']]
    return main(argv)
    
